package com.example.myapplication

import android.Manifest
import android.app.DownloadManager
import android.app.TaskInfo
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_agenda_detail.*

@Suppress("DEPRECATION")
class AgendaDetail : AppCompatActivity() {

    val STORAGE_PERMISSION_CODE : Int = 1000

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agenda_detail)

        val intent = intent
        var agenda = intent.getStringExtra("agenda")
        var tanggal = intent.getStringExtra("tanggal")
        var tempat = intent.getStringExtra("tempat")
        var waktu = intent.getStringExtra("waktu")
        var lampiran = intent.getStringExtra("lampiran")

        val agenda_holder = findViewById<TextView>(R.id.judul)
        val tempat_holder = findViewById<TextView>(R.id.tempat)
        val tanggal_holder = findViewById<TextView>(R.id.tanggal)
        val waktu_holder = findViewById<TextView>(R.id.waktu)

        agenda_holder.text = agenda
        tempat_holder.text = tempat
        tanggal_holder.text = tanggal
        waktu_holder.text = "Pukul " +  waktu

        buttonDl.setOnClickListener{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                    requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE_PERMISSION_CODE)
                }
                else{
                    Toast.makeText(this, "Downloading...", Toast.LENGTH_LONG).show()
                    startDownloading()
                }
            }
            else{
                Toast.makeText(this, "Downloading...", Toast.LENGTH_LONG).show()
                startDownloading()
            }
        }
    }

    fun startDownloading(){
        val Intent = intent
        var agenda = intent.getStringExtra("agenda")
        var lampiran = intent.getStringExtra("lampiran")
        val request = DownloadManager.Request(Uri.parse(lampiran.toString()))
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
        request.setTitle(agenda)
        request.setDescription(lampiran)
        request.allowScanningByMediaScanner()
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "${System.currentTimeMillis()}")
        //something shit
        val manager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        manager.enqueue(request)

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            STORAGE_PERMISSION_CODE-> {
                if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "Downloading...", Toast.LENGTH_LONG).show()
                startDownloading()
                }
                else{
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show()
                }
            }
        }
    }


}
