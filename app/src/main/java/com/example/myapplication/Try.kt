package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_try.*

class Try : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_try)

        checkBox.setOnCheckedChangeListener { compoundButton, b ->
            if(b){
                Toast.makeText(this, "hello", Toast.LENGTH_LONG).show()
            }
        }
    }
}
