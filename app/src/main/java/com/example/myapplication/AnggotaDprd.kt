package com.example.myapplication

data class AnggotaDprd(
    var nomor : String = "",
    var nama : String = "",
    var password : String = "",
    var jabatan : String = ""
)