package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val user = FirebaseAuth.getInstance().currentUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val intentUser = Intent(this, Index::class.java)
        val intentAdmin = Intent(this, AdminIndex::class.java)

        if(user != null){
            if(user.email.toString().equals("admin@gmail.com")){
                startActivity(intentAdmin)
            }
            else{
                startActivity(intentUser)
            }
        }

        button_login.setOnClickListener{
            val email = username.text.toString() + "@gmail.com"
            val pass = password.text.toString()

            if(email.isEmpty() || pass.isEmpty()){
            Toast.makeText(baseContext, "Masukan Email dan Password", Toast.LENGTH_SHORT).show();
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, pass).addOnCompleteListener(this){
                if (it.isSuccessful) {
                    if(email.equals("admin@gmail.com")){
                        Log.d("MainActivity", "signInWithEmail:success")
//                    saveUserToFirebaseDatabase()
                        intentUser.putExtra("email", email)
                        startActivity(intentAdmin)
                    }
                    else{
                        Log.d("MainActivity", "signInWithEmail:success")
//                    saveUserToFirebaseDatabase()
                        intentAdmin.putExtra("email", email)
                        startActivity(intentUser)
                    }
                }
                else{
                    Log.w("MainActivity", "signInWithEmail:failure", it.exception)
                    Toast.makeText(baseContext, "Kata Sandi dan Email Salah", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
//    fun saveUserToFirebaseDatabase(){
//        val uid = FirebaseAuth.getInstance().uid?:""
//        val ref = FirebaseDatabase.getInstance().getReference("users/$uid")
//        val user = User(uid, username.text.toString(), password.text.toString())
//
//        ref.setValue(user).addOnSuccessListener {
//            Log.d("MainActivity", "DATA SAVED")
//        }
//
//    }
}

