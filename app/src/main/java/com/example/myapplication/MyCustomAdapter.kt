package com.example.myapplication

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import kotlin.collections.ArrayList


class MyCustomAdapter(context: Context, var dataSource: ArrayList<Agenda>) : BaseAdapter(){
    private val mContext : Context

    init {
        mContext = context
    }

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(p0: Int): Any {
        return "test String"
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val layoutInFlater = LayoutInflater.from(mContext)
        val row_main = layoutInFlater.inflate(R.layout.main_row, p2, false)
        val row_agenda =row_main.findViewById<TextView>(R.id.judul)
        val row_tempat = row_main.findViewById<TextView>(R.id.tempat)
        val row_tanggal = row_main.findViewById<TextView>(R.id.tanggal)
        val row_waktu = row_main.findViewById<TextView>(R.id.jam)
        row_agenda.text = dataSource[p0].agenda
        row_tempat.text = dataSource[p0].tempat
        row_tanggal.text = dataSource[p0].tanggal
        row_waktu.text = "Pukul " + dataSource[p0].waktu

        return row_main


//           val textView = TextView(mContext)
//            textView.text = "here we go"
//            return textView
    }
}