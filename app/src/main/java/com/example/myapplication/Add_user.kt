package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_add_user.*
import kotlinx.android.synthetic.main.send_menu.*

class Add_user : AppCompatActivity() {
    var auth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_user)

        button_tambah.setOnClickListener{
            var nama = nama.text.toString()
            var nomor = nomor_hp.text.toString()
            var pass = password.text.toString()
            var jabatan : String = ""
            if(radioButton_anggota.isChecked){
                jabatan = "Anggota DPRD"
            }
            else if(radioButton_sekretariat.isChecked){
                jabatan = "Sekretariat DPRD"
            }
            else if(radioButton_skpd.isChecked){
                jabatan = "SKPD"
            }
            else if(radioButton_forkompinda.isChecked){
                jabatan = "Forkompinda"
            }
            else if(radioButton_vertikal.isChecked){
                jabatan = "Instansi Vertikal"
            }

            saveUserToFirebaseDatabase(nomor, nama, jabatan, pass)
            createUser(nomor, pass)
        }
    }

    fun saveUserToFirebaseDatabase(nomor : String, nama : String, jabatan : String, password: String){
        val uid = nomor
        val ref = FirebaseDatabase.getInstance().getReference("users/$jabatan/$nomor")
        val user = UserPush(nomor, nama, jabatan, password )

        ref.setValue(user).addOnSuccessListener {
            Log.d("MainActivity", "DATA SAVED")
        }
    }
    class UserPush (val nomor : String, val nama : String, val jabatan: String, val password: String)

    fun createUser(email : String, password : String){
        auth.createUserWithEmailAndPassword(email + "@gmail.com", password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("Add User", "createUserWithEmail:success")
                    Toast.makeText(baseContext, "Pengguna Ditambahkan.",
                        Toast.LENGTH_SHORT).show()
                    finish()
                    startActivity(getIntent())

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("Add User", "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                    finish()
                    startActivity(getIntent())
                }

                // ...
            }

    }
}
