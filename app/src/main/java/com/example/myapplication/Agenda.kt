package com.example.myapplication

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

data class Agenda(
    var agenda : String = "",
    var tempat : String = "",
    var tanggal : String = "",
    var waktu : String = "",
    var lampiran : String = ""
)