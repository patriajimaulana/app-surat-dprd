package com.example.myapplication

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_index.*
import kotlin.collections.ArrayList


@Suppress("DEPRECATION")
class Index : AppCompatActivity() {

    val fbAuth = FirebaseAuth.getInstance()
    val user = FirebaseAuth.getInstance().currentUser
    var agenda_list = ArrayList<Agenda>()
    val refA = FirebaseDatabase.getInstance().getReference("fraksia")
    val refB = FirebaseDatabase.getInstance().getReference("fraksib")
    val storage = FirebaseStorage.getInstance()
    val storageRef = storage.reference
    var anggotaDprd_numb = ArrayList<String>()
    var sekretariat_numb = ArrayList<String>()
    var skpd_numb = ArrayList<String>()
    var forkompinda_numb = ArrayList<String>()
    var instasi_vertikal = ArrayList<String>()


    //notif stuff

    lateinit var notificationManager : NotificationManager
    lateinit var notificationChannel : NotificationChannel
    lateinit var builder : Notification.Builder
    val channelId = "com.example.myapplication"
    val description = "Test notification"


    override fun onCreate(savedInstanceState: Bundle?) {
        var urlDl = ""
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_index)

        storageRef.child("/images/TUGAS IMK MEMBUAT INTERFACE.pdf").downloadUrl.addOnSuccessListener {
            Log.d("Index", "THIS IS YOUR FUCKING URL YO $it")
            urlDl = it.toString()
        }.addOnFailureListener{
            Log.d("Index", "FAILED YOOOOOOOO")
        }


        user?.let{
            var email = user.email
            var name = user.displayName
            Log.d("Index", "EMAIL MU NI COK" + email)
        }

        var email = user?.email.toString()
        val listView = findViewById<ListView>(R.id.agenda_view)
        email =  email.replace("@gmail.com", "")

        var ref_dprd_numb = FirebaseDatabase.getInstance().getReference("users/Anggota DPRD/")
        getAllNumbersDprd(ref_dprd_numb, anggotaDprd_numb, email)

        var ref_skpd_numb = FirebaseDatabase.getInstance().getReference("users/SKPD/")
        getAllNumbersSkpd(ref_skpd_numb, skpd_numb, email)

        var ref_sekretariat_numb = FirebaseDatabase.getInstance().getReference("users/Sekretariat DPRD/")
        getAllNumbersSekretariat(ref_sekretariat_numb, sekretariat_numb, email)

        var ref_forkompinda_numb = FirebaseDatabase.getInstance().getReference("users/Forkompinda/")
        getAllNumbersForkompinda(ref_forkompinda_numb, forkompinda_numb, email)

        var ref_instansi_vert = FirebaseDatabase.getInstance().getReference("users/Instansi Vertikal/")
        getAllNumbersVer(ref_instansi_vert, instasi_vertikal, email)


        Log.d("Index", "jumlah" +  anggotaDprd_numb.size)



        listView.setOnItemClickListener { adapterView, view, i, l ->
            val intent = Intent(this, AgendaDetail::class.java)
            intent.putExtra("agenda", agenda_list[i].agenda)
            intent.putExtra("tempat", agenda_list[i].tempat)
            intent.putExtra("tanggal", agenda_list[i].tanggal)
            intent.putExtra("waktu", agenda_list[i].waktu)
            intent.putExtra("lampiran", agenda_list[i].lampiran)
            startActivity(intent)

        }
    }

    //DONT TOUCH THIS
    fun getAllNumbersSkpd(ref : DatabaseReference, anggotaDprd_numb : ArrayList<String>, email : String){
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (productSnapshot in dataSnapshot.children) {
                    val agenda = productSnapshot.key.toString()
                    anggotaDprd_numb.add(agenda)
                }

                Log.d("Index", "email ni kon" + email )
                if(anggotaDprd_numb.contains(email)){
                    var refJab_dprd = FirebaseDatabase.getInstance().getReference("users/SKPD/$email/Agenda")
                    val listView = findViewById<ListView>(R.id.agenda_view)
                    showData(refJab_dprd, agenda_list, listView)
                    Log.d("TUREEEEE", "BISMILLAH")
                    jabatan.text = "SKPD"
                }
                else{
                    //
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                throw databaseError.toException()
                Log.d("Index", "Load Data Gagal")
            }
        })
    }

    fun getAllNumbersSekretariat(ref : DatabaseReference, anggotaDprd_numb : ArrayList<String>, email : String){
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (productSnapshot in dataSnapshot.children) {
                    val agenda = productSnapshot.key.toString()
                    anggotaDprd_numb.add(agenda)
                }

                Log.d("Index", "email ni kon" + email )
                if(anggotaDprd_numb.contains(email)){
                    var refJab_dprd = FirebaseDatabase.getInstance().getReference("users/Sekretariat DPRD/$email/Agenda")
                    val listView = findViewById<ListView>(R.id.agenda_view)
                    showData(refJab_dprd, agenda_list, listView)
                    Log.d("TUREEEEE", "BISMILLAH")
                    jabatan.text = "Sekretariat DPRD"
                }
                else{
                    //
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                throw databaseError.toException()
                Log.d("Index", "Load Data Gagal")
            }
        })
    }

    fun getAllNumbersForkompinda(ref : DatabaseReference, anggotaDprd_numb : ArrayList<String>, email : String){
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (productSnapshot in dataSnapshot.children) {
                    val agenda = productSnapshot.key.toString()
                    anggotaDprd_numb.add(agenda)
                }

                Log.d("Index", "email ni kon" + email )
                if(anggotaDprd_numb.contains(email)){
                    var refJab_dprd = FirebaseDatabase.getInstance().getReference("users/Forkompinda/$email/Agenda")
                    val listView = findViewById<ListView>(R.id.agenda_view)
                    showData(refJab_dprd, agenda_list, listView)
                    Log.d("TUREEEEE", "BISMILLAH")
                    jabatan.text = "Forkompinda"
                }
                else{
                    //
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                throw databaseError.toException()
                Log.d("Index", "Load Data Gagal")
            }
        })
    }
    fun getAllNumbersVer(ref : DatabaseReference, anggotaDprd_numb : ArrayList<String>, email : String){
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (productSnapshot in dataSnapshot.children) {
                    val agenda = productSnapshot.key.toString()
                    anggotaDprd_numb.add(agenda)
                }

                Log.d("Index", "email ni kon" + email )
                if(anggotaDprd_numb.contains(email)){
                    var refJab_dprd = FirebaseDatabase.getInstance().getReference("users/Instansi Vertikal/$email/Agenda")
                    val listView = findViewById<ListView>(R.id.agenda_view)
                    showData(refJab_dprd, agenda_list, listView)
                    Log.d("TUREEEEE", "BISMILLAH")
                    jabatan.text = "Instansi Vertikal"
                }
                else{
                    //
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                throw databaseError.toException()
                Log.d("Index", "Load Data Gagal")
            }
        })
    }




    fun getAllNumbersDprd(ref : DatabaseReference, anggotaDprd_numb : ArrayList<String>, email : String){
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (productSnapshot in dataSnapshot.children) {
                    val agenda = productSnapshot.key.toString()
                    anggotaDprd_numb.add(agenda)
                }

                Log.d("Index", "email ni kon" + email )
                if(anggotaDprd_numb.contains(email)){
                    var refJab_dprd = FirebaseDatabase.getInstance().getReference("users/Anggota DPRD/$email/Agenda")
                    val listView = findViewById<ListView>(R.id.agenda_view)
                    showData(refJab_dprd, agenda_list, listView)
                    Log.d("TUREEEEE", "BISMILLAH")
                    jabatan.text = "Anggota DPRD"
                }
                else{
                    //
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                throw databaseError.toException()
                Log.d("Index", "Load Data Gagal")
            }
        })
    }
    //DONT TOUCH THIS

    fun showData(ref : DatabaseReference, agenda_list : ArrayList<Agenda>, listView : ListView){
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                agenda_list.clear()
                for (productSnapshot in dataSnapshot.children) {
                    val agenda = productSnapshot.getValue(Agenda::class.java)
                    agenda_list.add(agenda!!)
                    listView.adapter = MyCustomAdapter(applicationContext, agenda_list)
                }
                if(agenda_list.size != 0){
                    getNotified()
                }
                Log.d("Index", "jumlah ni coy" +  agenda_list.size.toString())
                if (agenda_list.size == 0) {
                    nullAgenda.visibility = TextView.VISIBLE
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                throw databaseError.toException()
                Log.d("Index", "Load Data Gagal")
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.keluar -> {
            fbAuth.signOut()
            finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            true
        }
        else ->{
            super.onOptionsItemSelected(item)
        }

    }

    //back button pressed
    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            finishAffinity()
            return
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT).show()
        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }

    //notification
    fun getNotified(){

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val intent = Intent(applicationContext,Index::class.java)
        val pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = NotificationChannel(channelId,description,NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.WHITE
            notificationChannel.enableVibration(true )
            notificationManager.createNotificationChannel(notificationChannel)

            builder = Notification.Builder(this,channelId)
                .setContentTitle("Surat Masuk")
                .setContentText("Surat Masuk")
                .setSmallIcon(R.drawable.logo_kota)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.logo_kota))
                .setContentIntent(pendingIntent)
        }else{
            builder = Notification.Builder(this)
                .setContentTitle("Surat Masuk")
                .setContentText("Surat Masuk")
                .setSmallIcon(R.drawable.logo_kota)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.logo_kota))
                .setContentIntent(pendingIntent)
                .setLights(Color.RED, 3000, 3000)
        }
        notificationManager.notify(1234,builder.build())
    }
}

