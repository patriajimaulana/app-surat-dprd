package com.example.myapplication

import android.app.AlertDialog
import android.app.Application
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_admin_index.*
import kotlinx.android.synthetic.main.activity_index.*
import kotlinx.android.synthetic.main.send_anggota.*
import kotlinx.android.synthetic.main.send_anggota.view.*
import kotlinx.android.synthetic.main.send_anggota_list.*
import kotlinx.android.synthetic.main.send_anggota_list.view.*
import kotlinx.android.synthetic.main.send_menu.*
import kotlinx.android.synthetic.main.send_menu.view.*
import kotlinx.android.synthetic.main.send_menu.view.anggota
import java.text.SimpleDateFormat
import java.util.*
import kotlinx.android.synthetic.main.send_menu.view.title as title1

class AdminIndex : AppCompatActivity() {
    val fbAuth = FirebaseAuth.getInstance()
    var formate = SimpleDateFormat("dd MMM, YYYY",Locale.US)
    var timeFormat = SimpleDateFormat("hh:mm", Locale.US)
    val now = Calendar.getInstance()
    var tujuan = arrayListOf<String>()
    var penerima = arrayListOf<AnggotaDprd>()
    var jabatan : String  =""
    val refAnggotaDprd = FirebaseDatabase.getInstance().getReference("users/Anggota DPRD")
    val refSekretariatDprd = FirebaseDatabase.getInstance().getReference("users/Sekretariat DPRD")
    val refSkpd = FirebaseDatabase.getInstance().getReference("users/SKPD")
    val refIntansiVertikal = FirebaseDatabase.getInstance().getReference("users/Instansi Vertikal")
    val refForkompinda = FirebaseDatabase.getInstance().getReference("users/Forkompinda")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_index)
        Log.d("admin", "THE SIZEE : " + tujuan.size.toString())

        tanggal_button.setOnClickListener{
            val datePicker = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                val selectedDate = Calendar.getInstance()
                selectedDate.set(Calendar.YEAR,year)
                selectedDate.set(Calendar.MONTH,month)
                selectedDate.set(Calendar.DAY_OF_MONTH,dayOfMonth)
                val date = formate.format(selectedDate.time)
                Toast.makeText(this,date,Toast.LENGTH_SHORT).show()
                tanggal_field.text = date
            },
                    now.get(Calendar.YEAR),now.get(Calendar.MONTH),now.get(Calendar.DAY_OF_MONTH))
            datePicker.show()
        }

        waktu_button.setOnClickListener{
            try {
                if(waktu_button.text != "Show Dialog") {
                    val date = timeFormat.parse(waktu_button.text.toString())
                    now.time = date
                }
            }catch (e:Exception){
            e.printStackTrace()
        }
            val timePicker = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                val selectedTime = Calendar.getInstance()
                selectedTime.set(Calendar.HOUR_OF_DAY,hourOfDay)
                selectedTime.set(Calendar.MINUTE,minute)
                waktu_field.text = timeFormat.format(selectedTime.time)
            },
                now.get(Calendar.HOUR_OF_DAY),now.get(Calendar.MINUTE),true)
            timePicker.show()
        }
        lampiran_button.setOnClickListener {
            val intent = Intent()
                .setType("*/*")
                .setAction(Intent.ACTION_GET_CONTENT)

            startActivityForResult(Intent.createChooser(intent, "Select a file"), 111)
        }

        button_kirim.setOnClickListener{
            val sendDialogPopMenu = LayoutInflater.from(this).inflate(R.layout.send_menu, null)
            val mBuilder = AlertDialog.Builder(this)
                .setView(sendDialogPopMenu)
            val mAlertDialogMenu =  mBuilder.show()


            fun showReciever(ref: DatabaseReference, jabatan : String){
                mAlertDialogMenu.dismiss()
                val sendDialogPopAnggota = LayoutInflater.from(this).inflate(R.layout.send_anggota, null)
                val mBuilder = AlertDialog.Builder(this)
                    .setView(sendDialogPopAnggota)

                val mAlertDialogAnggota =  mBuilder.show()

                sendDialogPopAnggota.selesai.setOnClickListener {
                    mAlertDialogAnggota.dismiss()
                }


                sendDialogPopAnggota.theList.setOnItemClickListener { adapterView, view, i, l ->
                    if(tujuan.contains(penerima[i].nomor)){
                        tujuan.remove(penerima[i].nomor)
                        Toast.makeText(this, penerima[i].nama + " Dihapus " + "\nJumlah Penerima ${tujuan.size.toString()}", Toast.LENGTH_SHORT).show()
                    }
                    else{
                        tujuan.add(penerima[i].nomor)
                        Toast.makeText(this, penerima[i].nama + " Ditambahkan" +  "\nJumlah Penerima ${tujuan.size.toString()}" , Toast.LENGTH_SHORT).show()
                    }
                }

                showData(ref, penerima, sendDialogPopAnggota.theList)
                sendDialogPopAnggota.title.text = jabatan


                sendDialogPopAnggota.selesai.setOnClickListener {
                    Log.d("ANJING", tujuan.size.toString())
                }

           }

            //anggota DPRD pilih
            sendDialogPopMenu.anggota.setOnClickListener{
                showReciever(refAnggotaDprd, "Anggota DPRD")
                jabatan = "Anggota DPRD"
            }
            //sekretariat DPRD pilih
            sendDialogPopMenu.sekretariat.setOnClickListener{
                showReciever(refSekretariatDprd, "Sekretariat DPRD")
                jabatan = "Sekretariat DPRD"
            }
            //SKPD DPRD pilih
            sendDialogPopMenu.skpd.setOnClickListener{
                showReciever(refSkpd, "SKPD")
                jabatan = "SKPD"
            }
            //Farkompinda DPRD pilih
            sendDialogPopMenu.forkompinda.setOnClickListener{
                showReciever(refForkompinda, "Forkompinda")
                jabatan = "Forkompinda"
            }
            //vertikal DPRD pilih
            sendDialogPopMenu.instansi_vertikal.setOnClickListener{
                showReciever(refIntansiVertikal, "Instansi Vertikal")
                jabatan = "Instansi Vertikal"
            }

        }

        button_submit.setOnClickListener{
            uploadFile()
        }
    }


    var selectedFile : Uri? = null
    //catch selected file
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 111 && resultCode == RESULT_OK) {
            selectedFile = data?.data //The uri with the location of the file
            lampiran_field.text = selectedFile.toString()
        }
    }


    //upload lampiran fle
    fun uploadFile(){
        if(selectedFile == null) return

        var filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")

        ref.putFile(selectedFile!!)
            .addOnSuccessListener {
                Log.d("upload file", "file uploaded : ${it.metadata?.path}")
                Toast.makeText(this, "UPLOADED", Toast.LENGTH_LONG).show()

                ref.downloadUrl.addOnSuccessListener {
                    it.toString()
                    Log.d("upload file", "file location : $it")
                    sendAgenda(it.toString())
                    Log.d("admin", "THE SIZEE : " + tujuan.size.toString())
                    tujuan.clear()
                    Log.d("admin", "THE SIZEE : " + tujuan.size.toString())
                }
            }
            .addOnFailureListener{
                Toast.makeText(this, "fialed uploading file", Toast.LENGTH_LONG).show()
            }
    }

    fun sendAgenda(lampiranUrl : String){
        for(x in tujuan){
            val ref = FirebaseDatabase.getInstance().getReference("users/$jabatan/$x/Agenda/${agenda_field.text.toString()}")
            val agenda = Agenda(agenda_field.text.toString(), tempat_field.text.toString(), tanggal_field.text.toString(), waktu_field.text.toString(), lampiranUrl)
            ref.setValue(agenda)
                .addOnSuccessListener {
                    Log.d("Admin", "SUCESS BROOOOOO UPLOADED")
                    finish()
                    startActivity(getIntent())
                }
        }
    }

    //Menu bar
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu_admin, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.lihat_agenda -> {
            Toast.makeText(this, "COMING SOON", Toast.LENGTH_LONG).show()
            true
        }
        R.id.tambah_pengguna ->{
            val intent = Intent(this, Add_user::class.java)
            startActivity(intent)
            true
        }
        R.id.keluar -> {
            fbAuth.signOut()
            finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            true
        }
        else ->{
            super.onOptionsItemSelected(item)
        }

    }
    //back button pressed
    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            finishAffinity()
            return
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT).show()
        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }


    //show data
    fun showData(ref : DatabaseReference, anggota_dprd : ArrayList<AnggotaDprd>, listView : ListView){
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                anggota_dprd.clear()
                for (productSnapshot in dataSnapshot.children) {
                    val anggota_list = productSnapshot.getValue(AnggotaDprd::class.java)
                    anggota_dprd.add(anggota_list!!)
                    listView.adapter = CustomAdapterAnggotaDprd(applicationContext, anggota_dprd)
                }
                Log.d("Admin Index", "jumlah ni coy" +  anggota_dprd.size.toString())
                if (anggota_dprd.size == 0) {
                    nullAgenda.visibility = TextView.VISIBLE
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                throw databaseError.toException()
                Log.d("Index", "Load Data Gagal")
            }
        })
    }
}
